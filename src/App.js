import './App.css';
import Home from './pages/Home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import AppNavBar from './components/AppNavBar';



function App() {
  return (
    <Router>
        <>
          <AppNavBar/>
          <Container>
              <Switch>
                <Route exact path="/" component={Home}/>
              
              </Switch>
          </Container>
        </>
      </Router>
  );
}

export default App;
