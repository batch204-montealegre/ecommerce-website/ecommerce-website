import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom';


export default function AppNavBar(){

	return(
    <Navbar bg="light" expand="lg">
      <Container>
        <Link className="navbar-brand" to="/">Candy Store</Link>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
      {}

      <Nav className="ms-auto">
            <Link className="nav-link" to="/">Home</Link>
            
      </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

