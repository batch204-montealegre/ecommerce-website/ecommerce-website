import Banner from '../components/Banner'
import Highlights from '../components/Highlights'


export default function Home() {

    const data = {
        title: "Candy Corner",
        content: "Free shipping nationwide!",
        destination: "/products",
        label: "Order Now!"
    }

    return (
        <>
            <Banner dataProp= {data} />
            <Highlights />
        </>
    )
}